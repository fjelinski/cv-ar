report = """
- Buenos Aires 4.206 | 502.457
- Ciudad de Buenos Aires 557 | 140.527
- Catamarca 12 | 492
- Chaco 185 | 11.848
- Chubut 220 | 9.262
- Córdoba 1.668 | 64.579
- Corrientes 122 | 2100
- Entre Ríos 258 | 11.692
- Formosa 6 | 144
- Jujuy 51 | 17.285
- La Pampa 92 | 1.710
- La Rioja 65 | 6.624
- Mendoza 744 | 37.846
- Misiones -6 | 197
- Neuquén 271 | 15.354
- Río Negro 207 | 19.418
- Salta 80 | 16.464
- San Juan 10 | 1.302
- San Luis 277 | 3.614
- Santa Cruz 138 | 7.405
- Santa Fe 2.050 | 80.873
- Santiago del Estero 145 | 6.960
- Tierra del Fuego** 238 | 8.386
- Tucumán 1386 | 36.123
"""

import re

def split(delimiters, string, maxsplit=0):
    import re
    regexPattern = '|'.join(map(re.escape, delimiters))
    return re.split(regexPattern, string, maxsplit)

provincias = [
    "Buenos Aires",
    "CABA",
    "Catamarca",
    "Chaco",
    "Chubut",
    "Córdoba",
    "Corrientes",
    "Entre Ríos",
    "Formosa",
    "Jujuy",
    "La Pampa",
    "La Rioja",
    "Mendoza",
    "Misiones",
    "Neuquén",
    "Río Negro",
    "Salta",
    "San Juan",
    "San Luis",
    "Santa Cruz",
    "Santa Fe",
    "Santiago del Estero",
    "Tierra del Fuego",
    "Tucumán"
]
casos = [0] * len(provincias)

delimiters = "\n"
report = split(delimiters, report)

for row in report:
    
    # ignore if no province
    if "|" not in row:
        continue

    # remove dot
    row = row.replace('.', '')
    row = row.replace('*', '')

    # remove last part after |
    row = row.split(" | ")[0]

    # split by spaces
    delimiters = " "
    row_split = split(delimiters, row)

    # province name
    prov_name = ""
    for word in row_split[:-1]:
        prov_name += word + " "

    # replace with CABA
    if "Ciudad" in prov_name:
        prov_name = "CABA"

    # find prov and accumulate
    found = False
    for index, provincia in enumerate(provincias):
        if provincia in prov_name:
            found = True
            try:
                casos[index] += int(row_split[-1])
            except:
                print("warning:", row)
    if found == False:
        print("warning:", row)

    # print(row_split[:-1], prov_name)

casos_total = 0
for index, provincia in enumerate(provincias):
    # print(casos[index], provincia)
    print(casos[index])
    casos_total += casos[index]

print(casos_total, "total")




