import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.transforms as transforms
from matplotlib.transforms import offset_copy
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from matplotlib.patches import FancyBboxPatch
from matplotlib.ticker import StrMethodFormatter
from matplotlib.ticker import NullFormatter
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import PercentFormatter
import datetime
from datetime import timedelta
import urllib.request
import csv
import math
import numpy as np
import copy
import imageio

# Update data bases
update = True

# Time span
date_time_now = datetime.datetime.now()
date_time_now = datetime.datetime(date_time_now.year, date_time_now.month, date_time_now.day)
date_time_start = date_time_now - timedelta(days=20)

# Provinces and population
# Indec 2020: https://www.indec.gob.ar/ftp/cuadros/publicaciones/proyecciones_prov_2010_2040.pdf
# Malvinas: 3398 (censo 2016 https://falklandstimeline.files.wordpress.com/2018/01/falkland_islands_census_2016_-_report_without_data_tables.pdf)
countries = [
    ("Argentina", 45376763),
    ("Bs. As.", 17541141),
    ("CABA", 3075646), 
    ("Catam.", 415438),
    ("Chaco", 1204541),
    ("Chubut", 618994),
    ("Cba.", 3760450),
    ("Ctes.", 1120801),
    ("E. Ríos", 1385961),
    ("Formosa", 605193),
    ("Jujuy", 770881),
    ("La Pampa", 358428),
    ("La Rioja", 393531),
    ("Mendoza", 1990338),
    ("Misiones", 1261294),
    ("Neuq.", 664057),
    ("Río N.", 747610),
    ("Salta", 1424397),
    ("San Juan", 781217),
    ("San Luis", 508328),
    ("Sta. Cruz", 365698),
    ("Sta. Fe", 3536418),
    ("S. del E.", 978313),
    ("T del F.", 173432 + 3398), # Add Malvinas
    ("Tucumán", 1694656)
]

###############
# Code start
###############

def K_formatter(number, pos=None):
    magnitude = 0
    while abs(number) >= 1000:
        magnitude += 1
        number /= 1000.0
    return '%d%s' % (number, ['', 'K', 'M', 'B', 'T', 'Q'][magnitude])

dates = {}
for country in countries:
    dates[country] = []

total_cases = {}
for country in countries:
    total_cases[country] = []

total_deaths = {}
for country in countries:
    total_deaths[country] = []

death_ratio = {}
for country in countries:
    death_ratio[country] = []

total_recovered = {}
for country in countries:
    total_recovered[country] = []

total_tests = {}
for country in countries:
    total_tests[country] = []

# Download latest @infomapuche data and save it locally:
# https://docs.google.com/spreadsheets/d/16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA/edit#gid=0
# https://docs.google.com/spreadsheets/d/16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA/export?format=csv&id=16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA&gid=0
# https://github.com/SistemasMapache/Covid19arData
url3 = "https://docs.google.com/spreadsheets/d/16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA/export?format=csv&id=16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA&gid=0"
file_name3 = "data_arg.csv"
if update:
    urllib.request.urlretrieve(url3, file_name3)

# Parse argentina data 
with open(file_name3, 'r') as file:
    csv_reader = csv.reader(file, delimiter=',')
    for row in csv_reader:
        if row[0] == "fecha" or row[0] == "":
            continue

        r_date = row[0]
        r_day = row[1]
        r_day_quar = row[2]
        r_argentina = row[3]
        r_country = row[4]
        if r_country == "CÃ³rdoba":
            r_country = "Cba."
        if r_country == "NeuquÃ©n":
            r_country = "Neuq."
        if r_country == "Corrientes":
            r_country = "Ctes."
        if r_country == "RÃ­o Negro":
            r_country = "Río N."
        if r_country == "TucumÃ¡n":
            r_country = "Tucumán"
        if r_country == "Entre RÃ­os":
            r_country = "E. Ríos"
        if r_country == "Ciudad de Buenos Aires":
            r_country = "CABA"
        if r_country == "Buenos Aires":
            r_country = "Bs. As."
        if r_country == "Santa Fe":
            r_country = "Sta. Fe"
        if r_country == "Santa Cruz":
            r_country = "Sta. Cruz"
        if r_country == "Tierra Del Fuego":
            r_country = "T del F."
        if r_country == "Tierra del Fuego":
            r_country = "T del F."
        if r_country == "Santiago del Estero":
            r_country = "S. del E."
        if r_country == "Santiago Del Estero":
            r_country = "S. del E."
        if r_country == "Catamarca":
            r_country = "Catam."
        r_reg = row[5]
        r_total_cases = row[6]
        r_new_cases = row[7]
        if r_new_cases == '':
            r_new_cases = "0"
        r_total_deaths = row[8]
        if r_total_deaths == '':
            r_total_deaths = "0"
        r_new_deaths = row[9]
        if r_new_deaths == '':
            r_new_deaths = "0"
        r_total_recovered = row[10]
        if r_total_recovered == '':
            r_total_recovered = "0"
        r_tx_type = row[11]
        r_inform = row[12]

        if r_new_cases == "0" and r_new_deaths == "0":
            continue

        date_time_obj = datetime.datetime.strptime(r_date, '%d/%m/%Y')

        # Add to provinces
        country_found = False
        for country in countries:
            if r_country == country[0]:
                country_found = True
                if date_time_obj in dates[country]:
                    total_cases[country][-1] += int(r_new_cases)
                    total_deaths[country][-1] += int(r_new_deaths)
                    death_ratio[country][-1] = total_deaths[country][-1] / total_cases[country][-1]
                else:
                    dates[country].append(date_time_obj)
                    if total_cases[country]:
                        total_cases[country].append(
                            total_cases[country][-1] + int(r_new_cases))
                        total_deaths[country].append(
                            total_deaths[country][-1] + int(r_new_deaths))
                    else:
                        total_cases[country].append(int(r_new_cases))
                        total_deaths[country].append(int(r_new_deaths))
                    death_ratio[country].append(total_deaths[country][-1] / total_cases[country][-1])
        # if not country_found and (r_new_cases != "0" or r_new_deaths != "0") :
        #     print("country not found:", r_country, r_new_cases, r_new_deaths)

        # Add to argentina
        if date_time_obj in dates[countries[0]]:
            total_cases[countries[0]][-1] += int(r_new_cases)
            total_deaths[countries[0]][-1] += int(r_new_deaths)
            death_ratio[countries[0]][-1] = total_deaths[countries[0]][-1] / total_cases[countries[0]][-1]
        else:
            dates[countries[0]].append(date_time_obj)
            if total_cases[countries[0]]:
                total_cases[countries[0]].append(total_cases[countries[0]][-1] + int(r_new_cases))
                total_deaths[countries[0]].append(total_deaths[countries[0]][-1] +int(r_new_deaths))
            else:
                total_cases[countries[0]].append(int(r_new_cases))
                total_deaths[countries[0]].append(int(r_new_deaths))
            death_ratio[countries[0]].append(total_deaths[countries[0]][-1] / total_cases[countries[0]][-1])

# Download tests sheet @jorgealiaga
# https://docs.google.com/spreadsheets/d/1M7uDgWSfy6z1MNbC9FP6jTNgvY7XchJ0m-BfW88SKtQ/edit?usp=sharing
url4 = "https://docs.google.com/spreadsheets/d/e/2PACX-1vS5-67uMxlfDLSmYvxMcZDiSffCG2MeL75gSUvuDvPLgbpHwC3TAOwBPeBYoiRi-rT2Qkt9u5aCCQrL/pub?gid=0&single=true&output=csv"
file_name4 = "data_arg_tests.csv"
if update:
    urllib.request.urlretrieve(url4, file_name4)

# Init tests and recovered in argetina
for date in dates[countries[0]]:
    total_tests[countries[0]].append(0)
    total_recovered[countries[0]].append(0)

# Parse test data 
with open(file_name4, 'r') as file:
    csv_reader = csv.reader(file, delimiter=',')
    for row in csv_reader:
        if row[0] == "":
            continue
        if row[0][0] == "F" or row[0][0] == "P" or row[0][0] == "Y" or row[0][0] == "h" or row[0][0] == " ":
            continue

        r_date = row[0]
        r_recuperados = row[3] 
        if r_recuperados == "":
            r_recuperados = "0"
        r_tests_negativos = row[5]
        if r_tests_negativos == "":
            r_tests_negativos = "0"
        r_tests_totales = row[6]
        if r_tests_totales == "":
            r_tests_totales = "0"

        date_time_obj = datetime.datetime.strptime(r_date, '%d/%m/%Y')

        if date_time_obj in dates[countries[0]]:
            index = dates[countries[0]].index(date_time_obj)
            total_tests[countries[0]][index] += int(r_tests_totales)
            total_recovered[countries[0]][index] = int(r_recuperados)

# Ensure days with no test registered get value from previous day
for t, tests in enumerate(total_tests[countries[0]]):
    if total_tests[countries[0]][t] == 0 and t != 0:
        total_tests[countries[0]][t] = total_tests[countries[0]][t - 1]
for r, recovered in enumerate(total_recovered[countries[0]]):
    if total_recovered[countries[0]][r] == 0 and r != 0:
        total_recovered[countries[0]][r] = total_recovered[countries[0]][r - 1]

# Ensure data is accumulated to last date
if total_recovered[countries[0]][-1] == 0:
    total_recovered[countries[0]][-1] = total_recovered[countries[0]][-2]
if total_tests[countries[0]][-1] == 0:
    total_tests[countries[0]][-1] = total_tests[countries[0]][-2]

###############
# Filter dates
###############
orig_dates = copy.deepcopy(dates)
date_time_latest = datetime.datetime.strptime("2019-01-01", '%Y-%m-%d')
for country in countries:
    for i in range(len(orig_dates[country])):
        if orig_dates[country][i] > date_time_latest:
            date_time_latest = orig_dates[country][i]
for country in countries:
    if dates[country]:
        if dates[country][-1] != date_time_latest:
            dates[country].append(date_time_latest)
            total_cases[country].append(total_cases[country][-1])
            total_deaths[country].append(total_deaths[country][-1])
            if country[0] == "Argentina":
                total_deaths[country].append(total_deaths[country][-1])
                death_ratio[country].append(death_ratio[country][-1])

# Add item 0 to provinces with 0 cases
for country in countries:
    if not total_cases[country]:
        dates[country].append(date_time_latest)
        total_cases[country].append(0)
        total_deaths[country].append(0)

# Create all dates array
all_dates = [date_time_start + datetime.timedelta(days=x)
             for x in range(0, (date_time_latest-date_time_start).days + 2)]

# Sort countries
countries_by_cases = sorted(countries, key=lambda x: total_cases[x][-1])

##############################
##############################
# Plot A: Province proportion
##############################
##############################

# Set font
font = {'family': 'serif',
        'weight': 'normal',
        'size': 8}
plt.rc('font', **font)

# Create figure and set grid
fig, ax = plt.subplots(figsize=(8, 8))
ax.set_axisbelow(True)
plt.grid(axis='x', which='both', color='0.95')
plt.grid(axis='y', which='major', color='0.93')
plt.grid(axis='y', which='minor', color='0.96')

# shrink current axis by 10%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])

# Blended transform (used later)
trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)

# Create color map: jet, viridis, magma, coolwarm, inferno, plasma, cividis, rainbow
color_map = plt.cm.coolwarm(np.linspace(0,1,len(countries_by_cases)))
color_map_100 = plt.cm.coolwarm(np.linspace(0,1,100))

# Sort color map
countries_by_proportion = sorted(countries, key=lambda x: total_cases[x][-1] / x[1])
color_map_prop = []
for i, country in enumerate(countries_by_cases):
    color_map_prop.append(color_map[countries_by_proportion.index(country)])

# Note main colors
color_death = color_map[20]
color_rate = color_map[13]
color_back = color_map[8]
test_color = "royalblue"
positivity_color = "gray"

# Plot title
plt.title("Proporción de casos confirmados de COVID-19 en Argentina")

# Proportion variables
prov_dates = [] # dates (x axis)
y = []          # proportion (y axis)
labels = []     # provinces name (label)

# Remove Argentina from proportion (only provinces)
for i, country in enumerate(countries_by_proportion):
    if country[0] == "Argentina":
        countries_by_proportion.remove(country)

# Make room for each provices in y and store labels
for i, country in enumerate(countries_by_proportion):
        y.append([])
        labels.append(country[0])

# Store proportion in y for each province for each date (need to check back sometimes)
for date in all_dates[:-1]:
    # Add date to date array
    prov_dates.append(date)

    # Add value for each country in y
    for i, country in enumerate(countries_by_proportion):
        if country[0] != "Argentina":
        
            if date in dates[country]:
                index1 = dates[country].index(date)
                index2 = dates[countries[0]].index(date)
                proportion = 100 * total_cases[country][index1] / total_cases[countries[0]][index2]
                y[i].append(proportion)                
            else:
                # Search back for last value in previous date, if any
                last_date = 0
                for date2 in dates[country]:
                    if date2 > date:
                        break
                    last_date = date2
                # No last value, append 0
                if last_date == 0:
                    y[i].append(0)
                # We have a last value, append it
                else:
                    index1 = dates[country].index(last_date)
                    index2 = dates[countries[0]].index(date)
                    proportion = 100 * total_cases[country][index1] / total_cases[countries[0]][index2]
                    y[i].append(proportion)                

# y axis limits and label
plt.ylim(bottom=0, top=100)
ax.yaxis.set_major_formatter(PercentFormatter())
ax.set_ylabel("Proporción de casos confirmados de COVID-19")
# x axis limits and label
plt.xlim(left=mdates.date2num(all_dates[0]))
plt.xlim(right=mdates.date2num(all_dates[-1]))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m'))
plt.xticks(all_dates, rotation=45, ha='right', rotation_mode="anchor")
xticks = ax.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)

# Plot stack graph
y = np.vstack(y)
ax.stackplot(all_dates[:-1], y, #  {'zero', 'sym', 'wiggle', 'weighted_wiggle'}
              labels=labels, baseline='zero', colors=color_map[:-1],
              ec='white', lw=0.4, alpha=0.9)

# Plot central table
# Recover Argetina in proportion list
countries_by_proportion.append(countries[0])                
start = 0 # bottom of the table
end = 0.8 # upper limit of the table    
stacked_proportion = 0
for i, country in enumerate(countries_by_cases):
    
    if country[0] == "Argentina":

        text = "Argentina, "+ dates[country][-1].strftime('%d/%m') + ":"
        text += "\n(" + str(total_cases[country][-1]) + ") casos"
        text += "\n(" + str(total_deaths[country][-1]) + ") fallecidos"
        text += "\n(" + str(round(100 * death_ratio[country][-1], 1)) + "%) tasa letalidad"
        text += "\n(" + str(round(1000000 * total_deaths[country][-1] / country[1], 1)) + ") fallec./millón"
        text += "\n(" + str(total_tests[country][-1]) + ") tests lab."
        text += "\n(" + str(total_recovered[country][-1]) + ") recuperados"
        text += "\n(" + str(total_cases[country][-1] - total_recovered[country][-1] - total_deaths[country][-1]) + ") activos"

        ax.annotate(text,
                    xy=(dates[country][-1], 100),
                    xytext=(dates[country][-1] + timedelta(days=1.5), 0.9),
                    textcoords=trans,
                    bbox=dict(boxstyle="round",
                        color=color_back,
                        alpha=0.5,
                        ec="none"),
                    ha="left", va="center", fontsize=9)
    else:
        # Days since last new case        
        days = "-"
        if len(total_cases[country]) > 1:
            index_t = 1
            while total_cases[country][-index_t] <= total_cases[country][-index_t -1]:
                if len(total_cases[country]) > index_t:
                    index_t += 1
                else:
                    break
            days = all_dates[-2] - dates[country][-index_t]
            days = str(days.days)
        text = "(" + days + ") " + country[0]
        ax.text(dates[country][-1] + timedelta(days=1.5),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text,
                transform=trans,
                ha="left", va="center", fontsize=9)

        # Days since last death 
        days = "-"
        if total_deaths[country][-1] != 0:
            if len(total_deaths[country]) > 1:
                index_t = 1
                while total_deaths[country][-index_t] <= total_deaths[country][-index_t -1]:
                    if len(total_deaths[country]) > index_t + 1:
                        index_t += 1
                    else:
                        break
                days = all_dates[-2] - dates[country][-index_t]
                days = str(days.days)
        text_d = "(" + days + ")"
        ax.text(dates[country][-1] + timedelta(days=6.9),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text_d,
                transform=trans,
                ha="right", va="center", fontsize=9,
                bbox=dict(boxstyle="round",
                        # fc=(0.7, 0.7, 1.0),
                        color=color_back,
                        alpha=0.5,
                        ec="none"),)

        # Letalitly rate for each province
        if  total_cases[country][-1] != 0:
            death_ratio_str = str(round(100 * total_deaths[country][-1] / total_cases[country][-1], 1))
        else:
            death_ratio_str = "-"

        ax.text(dates[country][-1] + timedelta(days=5.2),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                death_ratio_str + "%", 
                transform=trans,
                zorder=24,
                fontsize=8, fontfamily='serif',
                color='dimgray',
                va='center', ha='center', rotation=-15,
                bbox=dict(boxstyle="round",
                        color='white',
                        alpha=0.8,
                        ec="none"))

# Label (uses same i)
text = "(" + "días\ns/casos" + ")"
ax.text(dates[country][-1] + timedelta(days=1.49),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.000,
        text, color='gray',
        transform=trans,
        ha="left", va="center", fontsize=8)
text_d = "(" + "días\ns/fall." + ")"
ax.text(dates[country][-1] + timedelta(days=7.1),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.000,
        text_d, color='gray',
        transform=trans,
        ha="right", va="center", fontsize=8)
text_l = "(" + "tasa\nletal." + ")"
ax.text(dates[country][-1] + timedelta(days=4.295),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.000,
        text_l, color='gray',
        transform=trans,
        ha="center", va="center", fontsize=8)

for i, country in enumerate(countries_by_proportion):

    # Provinces
    if country[0] != "Argentina": # and total_cases[country][-1] != 0
        proportion = 100 * total_cases[country][-1] / total_cases[countries[0]][-1]
        days_back = 3
        proportion_2 = y[i][-days_back]
        alpha = 0.8
        i_mult = 3.8
        i_base = 3.5

        # Duplicate cases
        for d, date in enumerate(all_dates[:-1]):
            
            curr_date = date
            week_date = curr_date - timedelta(days=7) 
            
            count = 100
            while curr_date not in dates[country]:
                curr_date = curr_date - timedelta(days=1) 
                count -= 1
                if count == 0:
                    break
            if count == 0:
                continue
            curr_index = dates[country].index(curr_date)

            count = 100
            while week_date not in dates[country]:
                week_date = week_date - timedelta(days=1) 
                count -= 1
                if count == 0:
                    break
            if count == 0:
                continue
            week_date = dates[country].index(week_date)

            curr_cases = total_cases[country][curr_index] 
            week_cases = total_cases[country][week_date] 

            if np.log(curr_cases) - np.log(week_cases) == 0:
                dupli_days = math.inf
            else:
                dupli_days = 7 * np.log(2) / (np.log(curr_cases) - np.log(week_cases))

            if not (d < 5 or d > 15):
                continue

            if dupli_days < 0:
                continue

            if dupli_days < 99:
                dupli_days = "{:2.0f}".format(dupli_days)
                dupli_days = int(dupli_days)
                color_back_dupli = color_map_100[-dupli_days]
            else:
                color_back_dupli = 'white'
                dupli_days = "+"
            
            color='white'
            color_font = 'dimgray'
            alpha_text = 1
            alpha_back_dupli = 0.3
            if dupli_days == "+":
                alpha_back_dupli = 0.2
                alpha_text = 0.3
            if date == all_dates[-2]:
                color=color_back_dupli
                color_font = 'black'
                alpha_back_dupli = 0.8

            ax.text(date + timedelta(days=0.5), i_base + i*i_mult,
                    dupli_days, zorder=10, alpha=alpha_text,
                    fontsize=8, fontfamily='serif', color=color_font,
                    va='center', ha='center',
                    bbox=dict(boxstyle="round",
                        color=color,
                        alpha=alpha_back_dupli,
                        ec="none"))

        # Horitzontal line
        plt.hlines(i_base + i*i_mult,
                   dates[country][-1] - timedelta(days=days_back + 10.5), 
                   dates[country][-1] - timedelta(days=days_back + 2),
                   color=list(reversed(color_map[:-1]))[i], ls='-', lw=1)

        # Population percentage
        text_pop = str(round(100 * country[1]/countries[0][1],1)) + "%" 
        ax.text(dates[country][-1] - timedelta(days=days_back + 10),
                i_base + i*i_mult, text_pop,
                ha="center", va="center", fontsize=9, zorder=20,
                bbox=dict(boxstyle="round",
                        color='white',
                        alpha=alpha,
                        ec="none"))

        # Province
        text_p = country[0]
        ax.text(dates[country][-1] - timedelta(days=days_back + 7),
                i_base + i*i_mult, text_p,
                ha="center", va="center", fontsize=9, zorder=20,
                bbox=dict(boxstyle="round",
                        color='white',
                        alpha=alpha,
                        ec="none"))

        # Cases/100k.
        text_cm = str(round(100000 * total_cases[country][-1] / country[1],1))
        ax.text(dates[country][-1] - timedelta(days=days_back + 4.3),
                i_base + i*i_mult, text_cm,
                ha="center", va="center", fontsize=9, zorder=20,
                bbox=dict(boxstyle="round",
                        color=color_map[:-1][i],
                        alpha=alpha,
                        ec="white"))

        # Cases percentage
        text_cas = str(round(proportion)) + "%" 
        ax.text(dates[country][-1] - timedelta(days=days_back + 2.2),
                i_base + i*i_mult, text_cas,
                ha="center", va="center", fontsize=9, zorder=20,
                bbox=dict(boxstyle="round",
                        color='white',
                        alpha=alpha,
                        ec="none"))

        # Arrow
        ax.annotate("",
                xy=(dates[country][-1] - timedelta(days=0.3), 
                    stacked_proportion + proportion_2/2 ),
                xytext=(dates[country][-1] - timedelta(days=days_back + 2),
                        i_base + i*i_mult),
                arrowprops=dict(arrowstyle="-",
                                color=list(reversed(color_map[:-1]))[i],
                                connectionstyle="arc,rad=-0.1",
                                ls='-', lw=1))

        # Marker
        plt.plot(dates[country][-1] - timedelta(days=0.3), 
                stacked_proportion + proportion_2/2,
                'o', color=list(reversed(color_map[:-1]))[i],
                alpha=1, markeredgecolor='black',
                markersize=5, markeredgewidth=0.8)
        stacked_proportion += proportion

# National cases/100k
cases_100k_Arg = 100000 * total_cases[countries[0]][-1] / countries[0][1]
cases_100k_prev = 0
for i, country in enumerate(countries_by_proportion):
    if country[0] != "Argentina":
        cases_100k = 100000 * total_cases[country][-1] / country[1]
        if cases_100k_prev < cases_100k_Arg < cases_100k:
            # horitzontal line
            plt.hlines(i_base + i*i_mult - i_mult/2,
                    dates[country][-1] - timedelta(days=days_back + 13 -1.5), 
                    dates[country][-1] - timedelta(days=days_back + 1),
                    color='black', ls='-', lw=0.7)
            # text
            text_p = countries[0][0] + ": " + str(round(cases_100k_Arg, 1)) + " casos/100 mil hab."
            ax.text(dates[country][-1] - timedelta(days=days_back + 13 -1.5),
                    i_base + i*i_mult - i_mult/2, text_p,
                    ha="center", va="top", fontsize=7, zorder=20,
                    rotation=90,
                    bbox=dict(boxstyle="round",
                            #color=color_back,
                            color=color_map[:-1][i],
                            alpha=0.8, ls='-', lw=0.7,
                            ec="gray"))
        cases_100k_prev = cases_100k

# Label (uses same i)
text =  "Prop.\npoblación"
ax.text(dates[country][-1] - timedelta(days=days_back + 10),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)
text =  "Nombre\nprovincia"
ax.text(dates[country][-1] - timedelta(days=days_back + 7),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)
text =  "Casos/\n100k hab."
ax.text(dates[country][-1] - timedelta(days=days_back + 4.3),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)
text =  "Prop.\ncasos"
ax.text(dates[country][-1] - timedelta(days=days_back + 2.2),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)
text =  "Dupl.\nen días"
ax.text(dates[country][-1] - timedelta(days=days_back - 1.5),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)
ax.text(dates[country][-1] - timedelta(days=days_back + 14),
        i_base + 0.6 + i*i_mult, text,
        color='white', ha="center", va="center", fontsize=8)

# Text sources below
text1 = 'Fuentes: https://www.argentina.gob.ar/coronavirus/informe-diario, https://www.argentina.gob.ar/coronavirus/medidas-gobierno\n'
text1 += 'Datos: https://github.com/SistemasMapache/Covid19arData (@infomapache), tests cargados por @jorgealiaga.\n'
text1 += 'Poblaciones basadas en proyecciones del INDEC al 2020 (Malvinas inluída en T. del F). (***) ln(2)/(ln (casos)-ln(casos_7d))*7.'
text2 = 'Imagen:\njuanfraire@gmail.com'
plt.text(0.1, 0.02, text1, fontsize=6,
         transform=plt.gcf().transFigure, ha="left", va="center")
plt.text(0.93, 0.02, text2, fontsize=6,
         transform=plt.gcf().transFigure, ha="right", va="center")

plt.savefig("figure-arg-prop.png", format='png', dpi=300)
plt.savefig("figure-arg-prop.pdf", format='pdf')
plt.savefig("figure-arg-prop.svg", format='svg')
plt.show()

#quit()

##############################
##############################
# Plot B: Daily Cases
##############################
##############################

# Recover original total cases
orig_total_cases = copy.deepcopy(total_cases)

# Set font
font = {'family': 'serif',
        'weight': 'normal',
        'size': 8}
plt.rc('font', **font)

fig, ax = plt.subplots(figsize=(8, 8))
ax.set_axisbelow(True)
plt.grid(axis='x', which='both', color='0.95')
plt.grid(axis='y', which='major', color='0.93')
plt.grid(axis='y', which='minor', color='0.96')

# Shrink current axis by 10%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])

# Blended transform
trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)

# y axis
plt.yscale("log")
plt.ylim(bottom=10, top=4500000)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:.0f}'))
ax.yaxis.set_major_formatter(FuncFormatter(K_formatter))
ax.yaxis.set_minor_formatter(NullFormatter())
ax.set_ylabel("Casos confirmados de COVID-19 acumulados (escala logarítmica) (*)")
# x axis
plt.xlim(left=mdates.date2num(all_dates[0]))
plt.xlim(right=mdates.date2num(all_dates[-1]))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m'))
plt.xticks(all_dates, rotation=45, ha='right', rotation_mode="anchor")
xticks = ax.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)

argentina_alpha = 1
argentina_marker = '-o'
argentina_linewidth = 3
argentina_markersize = 8
argentina_markeredgecolor = 'royalblue'

country_alpha = 1
country_marker = '-o'
country_linewidth = 2
country_markersize = 6

# others: jet, veridis, magma
color_map = plt.cm.coolwarm(np.linspace(0,1,len(countries_by_cases)))

color_death = color_map[20]
color_rate = color_map[13]
color_back = color_map[8]
test_color = "royalblue"
positivity_color = "gray"

for i, country in enumerate(countries_by_cases):

    if country[0] == "Argentina":

        # argentina line cases
        plt.plot(dates[country], total_cases[country],
                argentina_marker, label=country[0], color=color_map[i],
                alpha=argentina_alpha, linewidth=argentina_linewidth,
                markeredgecolor=argentina_markeredgecolor,
                markersize=argentina_markersize,
                zorder=10)

        # argentina recovered region
        total_recovered_death = [a + b for a,b in zip(total_recovered[country],total_deaths[country])]
        ax.fill_between(dates[country], total_cases[country], total_recovered_death,
                        color=color_map[i], alpha=0.05, zorder=1)
        plt.plot(dates[country], total_recovered[country],
                '--', color=color_map[i], alpha=0.4, linewidth=1,
                zorder=1)

        # argentina markers
        for j, date in enumerate(dates[country]):
            if j == 0 or date < all_dates[0]:
                continue

            #continue

            # new cases
            new_cases = str(orig_total_cases[country][j] - orig_total_cases[country][j-1])

            # we have new deaths
            if total_deaths[country][j] != total_deaths[country][j - 1]:

                # new deaths
                new_deaths = str(total_deaths[country][j] - total_deaths[country][j-1])

                # big death marker 
                plt.plot(dates[country][j], total_cases[country][j],
                    argentina_marker, label=country[0], 
                    color=color_death,
                    alpha=argentina_alpha, linewidth=argentina_linewidth,
                    markeredgecolor=argentina_markeredgecolor,
                    markersize=18,
                    zorder=11)
                
                # new death number inside marker
                new_deaths_font_size = 8
                if len(new_deaths) >=3:
                    new_deaths_font_size = 6
                ax.text(dates[country][j], total_cases[country][j],
                    "+" + new_deaths, 
                    fontsize=new_deaths_font_size, fontfamily='serif',
                    color='white',
                    va='center', ha='center',
                    zorder=12)

                # new cases number outside marker
                trans_offset = offset_copy(ax.transData, x=0, y=0.12, units='inches', fig=fig)
                ax.text(date, total_cases[country][j],
                        "+" + new_cases, 
                        transform=trans_offset,
                        fontsize=8, fontfamily='serif',
                        color='dimgray',
                        va='bottom', ha='right', rotation=-20)

                # new tests below marker
                if j < len(total_tests[country]) and total_tests[country][j] - total_tests[country][j-1] != 0:
                    if total_tests[country][j] > 0:
                        #spaces = "                           " #4
                        spaces = "            " #4
                        new_tests = total_tests[country][j] - total_tests[country][j-1]
                        text_tests = "+" + str(new_tests) + spaces
                        ax.text(date, total_cases[country][j],
                                text_tests, 
                                fontsize=8, fontfamily='serif',
                                color=test_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=20)
                        # positivity
                        positivity = 100 * (orig_total_cases[country][j] - orig_total_cases[country][j-1]) / new_tests

                        # affect by missing test data on 10/04
                        if date == datetime.datetime.strptime("10/04/2020", '%d/%m/%Y'):
                            positivity = 100 * (orig_total_cases[country][j] - orig_total_cases[country][j-2]) / new_tests

                        text_positivity = "{:.0f}%".format(positivity) + spaces + " "
                        ax.text(date + timedelta(days=0.4), total_cases[country][j],
                                text_positivity, 
                                fontsize=7, fontfamily='serif',
                                color=positivity_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=2)

            # we have no new deaths
            else:
                # new cases number outside marker
                trans_offset = offset_copy(ax.transData, x=0, y=0.06, units='inches', fig=fig)
                ax.text(date, total_cases[country][j],
                        "+" + new_cases, 
                        transform=trans_offset,
                        fontsize=8, fontfamily='serif',
                        color='dimgray',
                        va='bottom', ha='right', rotation=-15)
                
                # new tests below marker
                if j < len(total_tests[country]) and total_tests[country][j] - total_tests[country][j-1] != 0:
                    if total_tests[country][j] > 0:
                        spaces = "      "
                        new_tests = total_tests[country][j] - total_tests[country][j-1]
                        text_tests = "+" + str(new_tests) + spaces
                        ax.text(date, total_cases[country][j],
                                text_tests, 
                                fontsize=8, fontfamily='serif',
                                color=test_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=2,)
                        # positivity
                        positivity = 100 * (orig_total_cases[country][j] - orig_total_cases[country][j-1]) / new_tests
                        text_positivity = "{:.0f}%".format(positivity) + spaces + " "
                        ax.text(date + timedelta(days=0.4), total_cases[country][j],
                                text_positivity, 
                                fontsize=7, fontfamily='serif',
                                color=positivity_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=2)
            
            # death ratio circle on the back
            plt.plot(dates[country][j], total_cases[country][j],
                    argentina_marker, label=country[0], 
                    color=color_rate,
                    alpha=0.2,
                    markeredgecolor=argentina_markeredgecolor,
                    markersize= 20 * 100 * death_ratio[country][j],
                    zorder=1)

    # other country (provinces)
    else:
        # line
        plt.plot(dates[country], total_cases[country],
                country_marker, label=country[0], color=color_map[i],
                alpha=country_alpha, linewidth=country_linewidth,
                markeredgecolor='black', markersize=country_markersize)

        # first and last points
        plt.plot(dates[country][-1], total_cases[country][-1],
                'o', label=country[0], color=color_map[i], 
                alpha=1, linewidth=country_linewidth,
                markersize=country_markersize,  markeredgecolor='black')
        plt.plot(dates[country][0], total_cases[country][0],
                'o', label=country[0], color=color_map[i], 
                alpha=1, linewidth=country_linewidth, 
                markersize=country_markersize, markeredgecolor='black')

        # prvinces death markers
        for j, date in enumerate(dates[country]):
            if j == 0 or date < all_dates[0]:
                continue
            
            # we have new deaths
            if total_deaths[country][j] != total_deaths[country][j - 1]:

                # new deaths
                new_deaths = str(total_deaths[country][j] - total_deaths[country][j-1])

                # offset if overlap
                y_offset_data = 0
                for other_country in countries_by_cases:
                    if other_country == country:
                        continue

                    if date in dates[other_country]:
                        index = dates[other_country].index(date)
                        # also death here today
                        if total_deaths[other_country][index] != total_deaths[other_country][index - 1]:
                            # check if overlapped
                            x, y_1 = ax.transData.transform([mdates.date2num(date),total_cases[country][j]])
                            x, y_2 = ax.transData.transform([mdates.date2num(date),total_cases[other_country][index]])                
                            y_diff_pixels = y_1 - y_2
                            y_diff_points = y_diff_pixels * 72./fig.dpi

                            if y_diff_points > 0 and y_diff_points < 12 or (y_diff_points == 0 and country > other_country):
                                # compute postive data_offset
                                y_offset_points = (12 - y_diff_points) / 2
                                y_offset_pixels = y_offset_points / (72./fig.dpi)
                                x, y_new_data = ax.transData.inverted().transform([0, y_1 + y_offset_pixels])
                                y_offset_data = y_new_data - total_cases[country][j]

                            if y_diff_points > -12 and y_diff_points < 0 or (y_diff_points == 0 and country > other_country):
                                # compute negative data_offset
                                y_offset_points = - (12 + y_diff_points) / 2
                                y_offset_pixels = y_offset_points / (72./fig.dpi)
                                x, y_new_data = ax.transData.inverted().transform([0, y_1 + y_offset_pixels])
                                y_offset_data = y_new_data - total_cases[country][j]

                # big death marker 
                plt.plot(dates[country][j], total_cases[country][j] + y_offset_data,
                    argentina_marker, label=country[0], 
                    color=color_map[i], transform=ax.transData,
                    alpha=1, linewidth=country_linewidth,
                    markeredgecolor='black',
                    markersize=12,
                    zorder=11)

                # new death number inside marker
                color = 'white'
                if j < 15:
                    color = 'black'
                ax.text(dates[country][j], total_cases[country][j] + y_offset_data,
                    "+" + new_deaths, transform=ax.transData,
                    fontsize=5, fontfamily='serif',
                    color=color,
                    va='center', ha='center',
                    zorder=12)

    # text column on the right
    start = 0 # bottom
    end = 0.8   # up    

    if country[0] == "Argentina":

        text = "Argentina, "+ dates[country][-1].strftime('%d/%m') + ":"
        text += "\n(" + str(orig_total_cases[country][-1]) + ") casos"
        text += "\n(" + str(total_deaths[country][-1]) + ") fallecidos"
        text += "\n(" + str(round(100 * death_ratio[country][-1], 1)) + "%) tasa letalidad"
        text += "\n(" + str(round(1000000 * total_deaths[country][-1] / country[1], 1)) + ") fallec./millón"
        text += "\n(" + str(total_tests[country][-1]) + ") tests lab."
        text += "\n(" + str(total_recovered[country][-1]) + ") recuperados"
        text += "\n(" + str(orig_total_cases[country][-1] - total_recovered[country][-1] - total_deaths[country][-1]) + ") activos (**)"

        ax.annotate(text,
                    xy=(dates[country][-1], total_cases[country][-1]),
                    #xytext=(dates[country][-1] + timedelta(days=1.5), total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5), 0.9),
                    textcoords=trans,
                    bbox=dict(boxstyle="round",
                        color=color_back,
                        alpha=0.5,
                        ec="none"),
                    ha="left", va="center", fontsize=9)
    else:

        # cases and prov name
        total_cases_font_size = 9
        if orig_total_cases[country][-1] >= 10000:
            total_cases_font_size = 8
        if orig_total_cases[country][-1] >= 100000:
            total_cases_font_size = 7
        text = "(" + str(orig_total_cases[country][-1]) + ") " + country[0]
        ax.text(dates[country][-1] + timedelta(days=1.5),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text,
                transform=trans,
                ha="left", va="center", fontsize=total_cases_font_size)

        # new cases
        new_cases_font_size = 8
        if orig_total_cases[country][j] - orig_total_cases[country][j-1] >= 1000:
            new_cases_font_size = 7
        new_cases = str(orig_total_cases[country][j] - orig_total_cases[country][j-1])
        ax.text(dates[country][-1] + timedelta(days=5.1),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                "+" + new_cases, 
                transform=trans,
                zorder=24,
                fontsize=new_cases_font_size, fontfamily='serif',
                color='dimgray',
                va='center', ha='center', rotation=-15,
                bbox=dict(boxstyle="round",
                        color='white',
                        alpha=0.8,
                        ec="none"))

        # deaths
        total_deaths_font_size = 9
        if total_deaths[country][-1] >= 1000:
            total_deaths_font_size = 8
        if total_deaths[country][-1] >= 10000:
            total_deaths_font_size = 7
        text_d = "(" + str(total_deaths[country][-1]) + ")"
        ax.text(dates[country][-1] + timedelta(days=7.1),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text_d,
                transform=trans,
                ha="right", va="center", fontsize=total_deaths_font_size,
                bbox=dict(boxstyle="round",
                        # fc=(0.7, 0.7, 1.0),
                        color=color_back,
                        alpha=0.5,
                        ec="none"),)
    
    # arrow
    if country[0] == "Argentina":
            ax.annotate("",
                    xy=(dates[country][-1], total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5), total_cases[country][-1]),
                    # textcoords=trans,
                    arrowprops=dict(arrowstyle="-", color=color_map[i],
                                    connectionstyle="arc,rad=-0.1",
                                    ls='-', lw=1))
    else:
        ax.annotate("",
                    xy=(dates[country][-1], total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5),
                            start + (i + 0.5) * (end-start) / len(countries_by_cases)),
                    textcoords=trans,
                    arrowprops=dict(arrowstyle="-", color=color_map[i],
                                    connectionstyle="arc,rad=-0.1",
                                    ls='-', lw=1))

# label (uses same i)
text = "(" + "casos" + ") " + "provincia"
ax.text(dates[country][-1] + timedelta(days=1.5),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.008,
        text, color='gray',
        transform=trans,
        ha="left", va="center", fontsize=8)
text_d = "(" + "fall." + ")"
ax.text(dates[country][-1] + timedelta(days=6.9),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.008,
        text_d, color='gray',
        transform=trans,
        ha="right", va="center", fontsize=8)

plt.title("Casos confirmados de COVID-19 en Argentina")

# linea de eventos
events = [
    #("2020-04-16", "Ampl. def. de\ncaso (anosmia/disgeusia)"),
    ("2020-03-30", "Ampl. def. de\ncaso (local)"),
    ("2020-03-22", "Ampliación def.\nde caso (directo)"),
    ("2020-03-20", "Cuarentena\ntotal y\nobligatoria"),
    ("2020-03-16", "Cierre de\nfronteras"),
    ("2020-03-15", "Suspensión\nfútbol y\nclases"),
    ("2020-03-12", "Suspensión\nespectáculos"),
    ("2020-03-11", "Cuarentena\na viajeros"),
]

for i, event in enumerate(events):
    date_time_line = datetime.datetime.strptime(event[0], '%Y-%m-%d')
    if date_time_line < all_dates[0]:
        continue
    x_axis = 0.25
    ha = 'right'
    if date_time_line < all_dates[4]:
        ha = 'left'
        x_axis = 0.2
    y_axis = 0.97 - i * 0.055
    ax.axvline(date_time_line, color='gray', lw=1, ls=':')
    # highlight cuarentena start
    if event[0]=="2020-03-20":
        ax.axvline(date_time_line - timedelta(days=0.1), color='gray', lw=0.5, ls='-')
    textstr = event[1]
    ax.text(x_axis, y_axis, textstr,
            transform=ax.transAxes, 
            fontsize=8, fontfamily='serif',
            va='center', ha=ha)
            #va='center', ha='right') # hist
    ax.annotate("",
                xy=(mdates.date2num(date_time_line), y_axis), xycoords=trans,
                xytext=(x_axis, y_axis), textcoords=ax.transAxes, 
                arrowprops=dict(arrowstyle="<-", color='gray', ls='-', lw=1))

# Lejenda
x_axis = 0.17 
y_axis = 0.23
bb = transforms.Bbox([[x_axis-0.14, y_axis-0.205], [x_axis+0.14, y_axis+0.045]]) 
p_bbox = FancyBboxPatch((bb.xmin, bb.ymin),
            abs(bb.width), abs(bb.height),
            transform=ax.transAxes, 
            boxstyle="round,pad=0",
            fc='white',
            ec='gray',
            alpha=0.9,
            zorder=20)
ax.add_patch(p_bbox)

plt.plot(x_axis, y_axis,
    argentina_marker, 
    transform=ax.transAxes, 
    label=country[0], 
    color=color_death,
    alpha=argentina_alpha, linewidth=argentina_linewidth,
    markeredgecolor=argentina_markeredgecolor,
    markersize=18,
    zorder=22)

plt.plot(x_axis, y_axis,
    argentina_marker,
    label=country[0], 
    transform=ax.transAxes, 
    color=color_rate,
    alpha=0.2,
    markeredgecolor=argentina_markeredgecolor,
    markersize= 20 * 1.7,
    zorder=22)

spaces = "     " # tests
ax.text(x_axis, y_axis,
        "+T" + spaces, 
        transform=ax.transAxes, 
        fontsize=8, fontfamily='serif',
        color=test_color,
        va='top', ha='center',
        rotation=90,
        zorder=21)

spaces = "    "
ax.text(x_axis + 0.02, y_axis, # positivity
        "P%" + spaces, 
        transform=ax.transAxes,
        fontsize=7, fontfamily='serif',
        color=positivity_color,
        va='top', ha='center',
        rotation=90,
        zorder=24)

ax.text(x_axis, y_axis, "+F", 
    transform=ax.transAxes, 
    fontsize=8, fontfamily='serif',
    color='white',
    va='center', ha='center',
    zorder=23)

ax.text(x_axis - 0.02, y_axis + 0.01, "+C", 
    transform=ax.transAxes, 
    fontsize=8, fontfamily='serif',
    color='dimgray',
    va='bottom', ha='right',
    zorder=23, rotation=-15)

text = "+C: nuevos casos\n+F: nuevos fallecidos\n"
text += "+T: tests diarios\nP%: positividad\n"
text += "Circ. externo: tasa let.\n"
text += "Línea punt.: recuperados\n"
text += "Área rosa: activos (**)\n"
ax.text(x_axis, y_axis - 0.14, text,
        transform=ax.transAxes, 
        fontsize=8, fontfamily='serif',
        va='center', ha='center',
        zorder=23)

# # Duplicate info (toward the future)
# ax.text(0.5, 0.985, "Duplica en días (***):", transform=ax.transAxes, zorder=25,
#         fontsize=8, fontfamily='serif', color='dimgray', va='center', ha='center')
# plt.hlines(0.96, all_dates[0], all_dates[-1], transform=trans,
#             color='dimgray', ls='-', lw=0.7, zorder=19)
# for d, date in enumerate(all_dates[:-1]):
#     horizon = 7
#     index = dates[countries[0]].index(date)
#     new_cases = total_cases[countries[0]][index] - total_cases[countries[0]][index - horizon]
#     dupli_days = round(horizon * total_cases[countries[0]][index] / new_cases)
#     ax.text(date + timedelta(days=0.5), 0.96,
#             str(dupli_days), zorder=24,
#             transform=trans, fontsize=8, fontfamily='serif', color='dimgray',
#             va='center', ha='center',
#             bbox=dict(boxstyle="round", color='white', alpha=0.9, ec="none"))

# # Duplicate info (toward the past)
# ax.text(0.5, 0.985, "Duplicó en días (***):", transform=ax.transAxes, zorder=25,
#         fontsize=8, fontfamily='serif', color='dimgray', va='center', ha='center')
# plt.hlines(0.96, all_dates[0], all_dates[-1], transform=trans,
#             color='dimgray', ls='-', lw=0.7, zorder=19)

# for d, date in enumerate(all_dates[:-1]):

#     curr_index = dates[countries[0]].index(date)
#     curr_cases = total_cases[countries[0]][curr_index] 
#     index = curr_index

#     while index > 0:
#         if total_cases[countries[0]][index] < curr_cases / 2:
#             break
#         index -= 1
#     if index != 0:
#         dupli_days = curr_index - index
#         ax.text(date + timedelta(days=0.5), 0.96,
#                 str(dupli_days), zorder=24,
#                 transform=trans, fontsize=8, fontfamily='serif', color='dimgray',
#                 va='center', ha='center',
#                 bbox=dict(boxstyle="round", color='white', alpha=0.9, ec="none"))

# Duplicate info (toward the past -  log version)
ax.text(0.5, 0.985, "Duplica en días (***):", transform=ax.transAxes, zorder=25,
        fontsize=8, fontfamily='serif', color='dimgray', va='center', ha='center')
plt.hlines(0.96, all_dates[0], all_dates[-1], transform=trans,
            color='dimgray', ls='-', lw=0.7, zorder=19)

for d, date in enumerate(all_dates[:-1]):
    # ln(2)/ (ln (casos hoy) - ln (casos hace 7 días)) * 7
    curr_index = dates[countries[0]].index(date)
    curr_cases = orig_total_cases[countries[0]][curr_index] 
    week_cases = orig_total_cases[countries[0]][curr_index - 7] 

    dupli_days = 7 * np.log(2) / (np.log(curr_cases) - np.log(week_cases))
    ax.text(date + timedelta(days=0.5), 0.96,
            "{:2.0f}".format(dupli_days), zorder=24,
            transform=trans, fontsize=8, fontfamily='serif', color='dimgray',
            va='center', ha='center',
            bbox=dict(boxstyle="round", color='white', alpha=0.9, ec="none"))

# White rect
date_time_line = datetime.datetime.strptime("2020-03-20", '%Y-%m-%d')
left, bottom = mdates.date2num(date_time_line), 1
width, height = mdates.date2num(all_dates[-1]) - mdates.date2num(date_time_line), plt.ylim()[1]
ax.add_patch(plt.Rectangle((left, bottom), width, height,
            facecolor="lavender", alpha=0.1, zorder=-10))

# Text sources below
text1 = 'Fuentes: https://www.argentina.gob.ar/coronavirus/informe-diario, https://www.argentina.gob.ar/coronavirus/medidas-gobierno\n'
text1 += 'Datos: https://github.com/SistemasMapache/Covid19arData (@infomapache), tests cargados por @jorgealiaga.\n'
text1 += '(*) Casos acumulados (recuperados, activos, y fallecidos), (**) Activos = casos - recuperados - fallecidos, (***) ln(2)/(ln (casos)-ln(casos_7d))*7.'
text2 = 'Imagen:\njuanfraire@gmail.com\n '
plt.text(0.1, 0.02, text1, fontsize=6,
         transform=plt.gcf().transFigure, ha="left", va="center")
plt.text(0.93, 0.02, text2, fontsize=6,
         transform=plt.gcf().transFigure, ha="right", va="center")

plt.savefig("figure-arg-cases.png", format='png', dpi=300)
plt.savefig("figure-arg-cases.pdf", format='pdf')
plt.savefig("figure-arg-cases.svg", format='svg')
plt.show()